<?php

/**
 * Meta Data
 *
 * "Project-Id-Version: Faq"
 * "Last-Translator: Filippo Baruffaldi (aka mdb)"
 * "Language-Team: IT (Nectarine.it)"
 * "MIME-Version: 1.0"w
 * "Content-Type: text/plain; charset=UTF-8"
 * "Content-Transfer-Encoding: 8bit"
 */

// Info
define('_IT_FAQ_NAME', "FAQ");
define('_IT_FAQ_INFO_DESCRIPTION', "Deposito di Frequently Asked Questions e delle loro risposte.");

// ACL Stuff
define('_IT_FAQ_ACL_DEFAULT', "Amministrazione F.A.Q.");
define('_IT_FAQ_ACL_ADDQUESTION', "Aggiungi domanda");
define('_IT_FAQ_ACL_EDITQUESTION', "Modifica domanda");
define('_IT_FAQ_ACL_DELETEQUESTION', "Cancella domanda");
define('_IT_FAQ_ACL_MANAGECATEGORIES', "Gestisci categorie");

// Strings
define('_IT_FAQ_LAYOUT_LISTCATEGORIES', "Lista categorie");
define('_IT_FAQ_LAYOUT_LISTCATEGORIES_DESCRIPTION', "Mostra una lista delle categorie di F.A.Q.");
define('_IT_FAQ_CATEGORIES', "Categorie F.A.Q.");
define('_IT_FAQ_TITLE', "Frequently Asked Questions");
define('_IT_FAQ_CONTENTS', "Contenuto");
define('_IT_FAQ_LIST', "Lista F.A.Q.");
define('_IT_FAQ_ADD_QUESTION', "Aggiungi domanda");
define('_IT_FAQ_ADD_CATEGORY', "Aggiungi categoria");
define('_IT_FAQ_ALL_CATEGORIES', "Tutte le categorie");
define('_IT_FAQ_CATEGORY', "Categoria");
define('_IT_FAQ_CONFIRM_DELETE_CATEGORY', "Sei sicuro di voler cancellare questa categoria e tutte le domande al suo interno?");
define('_IT_FAQ_EDIT_CATEGORY', "Modifica categoria");
define('_IT_FAQ_DELETE_CATEGORY', "Cancella categoria");
define('_IT_FAQ_QUESTION', "Domanda");
define('_IT_FAQ_MOVEUP', "Muovi su");
define('_IT_FAQ_MOVEDOWN', "Muovi giu'");
define('_IT_FAQ_CONFIRM_DELETE_QUESTION', "Sei sicuro di voler cancellare questa domanda?");
define('_IT_FAQ_START_ADD', "Comincia ad aggiungere domande in questa categoria");
define('_IT_FAQ_STATUS', "Stato");
define('_IT_FAQ_ACTIVE', "Attiva");
define('_IT_FAQ_INACTIVE', "Inattiva");
define('_IT_FAQ_UPDATE_QUESTION', "Aggiorna domanda");
define('_IT_FAQ_UPDATE_CATEGORY', "Aggiorna categoria");

// Responses
define('_IT_FAQ_QUESTION_ADDED', "Una nuova domanda F.A.Q. � stata aggiunta");
define('_IT_FAQ_QUESTION_UPDATED', "La domanda F.A.Q � stata aggiornata");
define('_IT_FAQ_QUESTION_DELETED', "La domanda F.A.Q � stata cancellata");
define('_IT_FAQ_QUESTION_MOVED', "La domanda F.A.Q � stata spostata");
define('_IT_FAQ_CATEGORY_MOVED', "La categoria F.A.Q � stata spostata");
define('_IT_FAQ_CATEGORY_ADDED', "Una nuova categoria F.A.Q � stata aggiunta");
define('_IT_FAQ_CATEGORY_UPDATED', "La categoria F.A.Q � stata aggiornata");
define('_IT_FAQ_CATEGORY_DELETED', "La categoria F.A.Q � stata cancellata");

// Errors
define('_IT_FAQ_ERROR_QUESTION_NOT_ADDED', "C'e' stato un problema aggiungendo la domanda F.A.Q.");
define('_IT_FAQ_ERROR_QUESTION_NOT_UPDATED', "C'e' stato un problema aggiornando la domanda F.A.Q.");
define('_IT_FAQ_ERROR_QUESTION_NOT_DELETED', "C'e' stato un problema cancellando la domanda F.A.Q.");
define('_IT_FAQ_ERROR_QUESTION_DOES_NOT_EXISTS', "La domanda F.A.Q. non esiste.");
define('_IT_FAQ_ERROR_QUESTION_NOT_MOVED', "C'e' stato un problema spostando la domanda F.A.Q.");
define('_IT_FAQ_ERROR_CATEGORY_DOES_NOT_EXISTS', "La categoria F.A.Q. non esiste.");
define('_IT_FAQ_ERROR_CATEGORY_NOT_ADDED', "C'e' stato un problema aggiungendo la categoria F.A.Q.");
define('_IT_FAQ_ERROR_CATEGORY_NOT_DELETED', "C'e' stato un problema cancellando la categoria F.A.Q.");
define('_IT_FAQ_ERROR_CATEGORY_NOT_UPDATED', "C'e' stato un problema aggiornando la categoria F.A.Q.");
define('_IT_FAQ_ERROR_CATEGORY_NOT_MOVED', "C'e' stato un problema spostando la categoria F.A.Q.");


?>
